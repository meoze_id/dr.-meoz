package com.meoze.drmeoz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * A fragment representing a single Disease detail screen.
 * This fragment is either contained in a {@link DiseaseListActivity}
 * in two-pane mode (on tablets) or a {@link DiseaseDetailActivity}
 * on handsets.
 */
public class DiseaseDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy name this fragment is presenting.
     */
    private Disease mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DiseaseDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy name specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load name from a name provider.
            mItem = DiseaseListFragment.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);
            }

            FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            mItem.getName() + "\n"
                                    + mItem.getDescription()
                                    + "\n#dr. Meoz");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
            });

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_disease_detail, container, false);

        // Show the dummy name as text in a TextView.
        if (mItem != null) {
//            ((TextView) rootView.findViewById(R.id.disease_detail)).setText(mItem.description);
            String text = "<html><body>"
                    + "<p align=\"justify\">"
                    + mItem.description
                    + "</p> "
                    + "</body></html>";
            ((WebView) rootView.findViewById(R.id.disease_detail)).loadData(text, "text/html", "utf-8");
        }

        return rootView;
    }
}
