package com.meoze.drmeoz.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample name for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DiseaseContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<Disease> ITEMS = new ArrayList<Disease>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, Disease> ITEM_MAP = new HashMap<String, Disease>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Disease item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static Disease createDummyItem(int position) {
        return new Disease(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore description information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of name.
     */
    public static class Disease {
        public String id;
        public String name;
        public String description;

        public Disease(String id, String name, String description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
